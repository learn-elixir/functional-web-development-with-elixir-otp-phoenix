defmodule IslandsEngine.Guesses do
  alias IslandsEngine.Guesses

  @enforce_keys [:hits, :misses]
  defstruct [:hits, :misses]

  def new(), do:
    %Guesses{hits: MapSet.new(), misses: MapSet.new()}
end
